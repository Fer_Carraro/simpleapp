package com.codemonkey;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;

import com.codemonkey.component.Saludar;
import com.codemonkey.service.Servicio;
import com.codemonkey.service.impl.ServicioImpl;
import com.codemonkey.service.MyService;
import com.codemonkey.service.impl.MyServiceImpl;
import com.codemonkey.clases.Camisa;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleAppApplicationTests {

	@Autowired
    @Qualifier("servicioImpl")
    Servicio servicioImpl;

	@Test
	public void testA() {
		assertEquals("Arrivaderci!",new Saludar().decirAdios());
	}

	@Test
	public void testB(){
		assertEquals(true, servicioImpl.isValido('R'));
	}

	@Test
	public void testC(){
		MyService myService = new MyServiceImpl();
		Camisa camisa = new Camisa();
		camisa.setTalla("MEDIANA");
		assertEquals(false,myService.isNotGrande(camisa));
	}

	@Test
	public void testD(){
		MyService myService = new MyServiceImpl();
		Camisa camisa = new Camisa();
		camisa.setTalla("MEDIANA");
		assertTrue(!myService.isNotGrande(camisa));
	}

}