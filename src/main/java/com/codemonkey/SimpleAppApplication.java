package com.codemonkey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import com.codemonkey.component.Saludar;
import com.codemonkey.component.InyectarPorConstructorServicio;
import com.codemonkey.component.InjectedByContsructorService;
import com.codemonkey.component.SetterBasedService;
import com.codemonkey.config.AppConfig;
import com.codemonkey.service.MyService;
import com.codemonkey.clases.Camisa;
import com.codemonkey.controller.GreetingController;



//@ComponentScan("com.codemonkey")
//@ImportResource("classpath:/springcurso/spring-config.xml")
@SpringBootApplication
public class SimpleAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleAppApplication.class, args);
		
		
		//ApplicationContext ctx = SpringApplication.run(SpringmvcApplication.class, args);
//        for (String name : ctx.getBeanDefinitionNames()){
//            System.out.println(name);
//        }
//        System.out.println("******* Bean Count *******");
//        System.out.println(ctx.getBeanDefinitionCount());


		
		//mockDemoA(args);
		//mockDemoB(args);
		//mockDemoC(args);
		//mockDemoD();
		//mockDemoE(args);
		//mockDemoF(args);
		//mockDemoG(args);
	}

	public static void mockDemoG(String[] argum){
		ApplicationContext context = SpringApplication.run(SimpleAppApplication.class, argum);
		GreetingController controller = (GreetingController) context.getBean("greetingController");
		controller.saludar();
	}

	public static void mockDemoF(String[] argum){
		ApplicationContext context = SpringApplication.run(SimpleAppApplication.class, argum);
		GreetingController controller = (GreetingController) context.getBean("greetingController");
		controller.decirAdios();
	}

	public static void mockDemoE(String[] argum){
		ApplicationContext context = SpringApplication.run(SimpleAppApplication.class, argum);
		GreetingController greetingControl = (GreetingController) context.getBean("greetingController");
		greetingControl.decirHola();
	}

	public static void mockDemoD(){
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        MyService obj = (MyService) context.getBean("myServiceBean");
        Camisa camisa = new Camisa();
        camisa.setColor("Roja");
        camisa.setTalla("GRANDE");
        camisa.setMarca("Thompson");
        String cad = obj.isNotGrande(camisa)? "Camisa de talla mediana o chica" : "Camisa de talla "+camisa.getTalla();
        System.out.println(cad);
	}

	public static void mockDemoC(String[] argum) {
        ApplicationContext ctx = SpringApplication.run(SimpleAppApplication.class, argum);
        InjectedByContsructorService contsructorService = (InjectedByContsructorService) ctx.getBean("injectedByContsructorService");

        contsructorService.getMessage();

        SetterBasedService setterBasedService = (SetterBasedService) ctx.getBean("setterBasedService");

        setterBasedService.getMessage();
    }

	public static void mockDemoB(String[] argum){
		ApplicationContext context = SpringApplication.run(SimpleAppApplication.class, argum);
		InyectarPorConstructorServicio inyectarPorConstructorServicio =  (InyectarPorConstructorServicio)context.getBean("inyectarPorConstructorServicio");
		inyectarPorConstructorServicio.getMensaje("Fernando Carraro Aguirre");
	}

	public static void mockDemoA(String[] argum){
		ApplicationContext context = SpringApplication.run(SimpleAppApplication.class, argum);
		Saludar saludar = (Saludar)context.getBean("saludar");
		saludar.diHola();
	}

}