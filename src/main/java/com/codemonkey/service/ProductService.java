package com.codemonkey.service;

import com.codemonkey.domain.Product;

public  interface ProductService extends CRUDService<Product> {

}