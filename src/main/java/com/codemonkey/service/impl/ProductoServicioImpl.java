package com.codemonkey.service.impl;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.*;

import com.codemonkey.service.ProductoServicio;
import com.codemonkey.clases.Producto;

@Service
public class ProductoServicioImpl implements ProductoServicio {

    private Map<Integer,Producto> productos;

    public ProductoServicioImpl() {
        loadProducts();
    }

    @Override
    public List<Producto> getAllProductos() {
        return new ArrayList<>(productos.values());
    }

    @Override 
    public Producto getProductoById(Integer id){
        return productos.get(id);
    }

    @Override
    public Producto saveOrUpdateProducto(Producto producto) {
        if (producto != null){
            if (producto.getId() == null){
                producto.setId(getNextKey());
            }
            productos.put(producto.getId(), producto);

            return producto;
        } else {
            throw new RuntimeException("Ha ocurrido un error al insertar producto");
        }
    }

    @Override
    public void borrarProducto(Integer id){
        productos.remove(id);
    }

    private Integer getNextKey(){
        return Collections.max(productos.keySet()) + 1;
    }

    private void loadProducts(){
        productos = new HashMap<>();

        Producto product1 = new Producto();
        product1.setId(1);
        product1.setDescripcion("Producto 1");
        product1.setPrecio(new BigDecimal("12.99"));
        product1.setImagenUrl("http://example.com/producto1");

        productos.put(1, product1);

        Producto product2 = new Producto();
        product2.setId(2);
        product2.setDescripcion("Producto 2");
        product2.setPrecio(new BigDecimal("14.99"));
        product2.setImagenUrl("http://example.com/producto2");

        productos.put(2, product2);

        Producto product3 = new Producto();
        product3.setId(3);
        product3.setDescripcion("Producto 3");
        product3.setPrecio(new BigDecimal("34.99"));
        product3.setImagenUrl("http://example.com/producto3");

        productos.put(3, product3);

        Producto product4 = new Producto();
        product4.setId(4);
        product4.setDescripcion("Producto 4");
        product4.setPrecio(new BigDecimal("44.99"));
        product4.setImagenUrl("http://example.com/producto4");

        productos.put(4, product4);

        Producto product5 = new Producto();
        product5.setId(5);
        product5.setDescripcion("Product 2");
        product5.setPrecio(new BigDecimal("25.99"));
        product5.setImagenUrl("http://example.com/producto5");

        productos.put(5, product5);
    }
}