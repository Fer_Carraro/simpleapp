package com.codemonkey.service.impl;

import com.codemonkey.service.ComponenteSaludo;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Tambien se podria usar:
 * @Profile("default")
 * @Profile({"default","german"})
 */


@Component
@Profile("german")
public class ComponenteSaludoAleman implements ComponenteSaludo{
    @Override public String adios(){
        return "Auf Wiedersehen!";
    }
}