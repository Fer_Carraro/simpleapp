package com.codemonkey.service.impl;

import static java.lang.System.out;
import com.codemonkey.service.HolaMundoServicio;
import org.springframework.stereotype.Service;

@Service("holaMundoServicioImpl")
public class HolaMundoServicioImpl implements HolaMundoServicio{
	@Override 
	public void decirHola(String nombre){
		out.println("Hola, "+nombre+"!!");
	}
}