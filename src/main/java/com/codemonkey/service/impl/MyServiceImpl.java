package com.codemonkey.service.impl;

import com.codemonkey.service.MyService;
import com.codemonkey.clases.Camisa;

public class MyServiceImpl implements MyService{

	@Override public boolean isNotGrande(Camisa camisa){
		return "GRANDE".equals(camisa.getColor());
	}
	
}