package com.codemonkey.service.impl;

import com.codemonkey.service.GreetingService;
import org.springframework.stereotype.Service;

@Service("greetingServiceImpl")
public class GreetingServiceImpl implements GreetingService{
    @Override public String getGreeting(){
        return "Hola desde GreetingServiceImpl !!!";
    }
}