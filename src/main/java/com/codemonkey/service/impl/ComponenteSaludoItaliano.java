package com.codemonkey.service.impl;

import com.codemonkey.service.ComponenteSaludo;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("italian")
public class ComponenteSaludoItaliano implements ComponenteSaludo{
    @Override public String adios(){
        return "Ciao!";
    }
}