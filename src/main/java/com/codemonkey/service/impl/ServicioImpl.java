package com.codemonkey.service.impl;

import com.codemonkey.service.Servicio;
import org.springframework.stereotype.Service;

@Service("servicioImpl")
public class ServicioImpl implements Servicio{
	
	@Override 
	public boolean isValido(char car){
		boolean salida = new Boolean("false");
		if(car == 'R'){
			salida = true;
		}else if(car == 'G'){
			salida = true;
		}else if(car == 'B'){
			salida = true;
		}
		return salida;
	}
}