package com.codemonkey.service.impl;

import org.springframework.stereotype.Component;
import com.codemonkey.service.HelloWorldService;

/**
 * Created by jt on 3/11/15.
 */
@Component
public class HelloWorldImpl implements HelloWorldService{
    public void sayHello(){
        System.out.println("Hola, mundo!!\nBienvenidos al curso de Spring Framework!!");
    }
}

