package com.codemonkey.service;

import java.util.List;

public interface CRUDService<T> {
    public abstract List<?> listAll();
    public abstract T getById(Integer id);
    public abstract T saveOrUpdate(T domainObject);
    public abstract void delete(Integer id);
}