package com.codemonkey.service;

public interface GreetingService{
    public abstract String getGreeting();
}