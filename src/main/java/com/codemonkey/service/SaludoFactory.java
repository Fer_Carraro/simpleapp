package com.codemonkey.service;

import com.codemonkey.service.ServicioSaludo;
import com.codemonkey.service.impl.ServicioSaludoAleman;
import com.codemonkey.service.impl.ServicioSaludoFrances;
import com.codemonkey.service.impl.ServicioSaludoItaliano;
import com.codemonkey.service.impl.ServicioSaludoPortugues;

public class SaludoFactory{
    public ServicioSaludo crearSaludoServicio(String lenguaje){
        ServicioSaludo servicio = null;
        switch(lenguaje){
            case "de": 
            servicio = new ServicioSaludoAleman();
            break;

            case "pt":
            servicio = new ServicioSaludoPortugues();
            break;

            case "fr":
            servicio = new ServicioSaludoFrances();
            break;

            case "it":
            servicio = new ServicioSaludoItaliano();
            break;

            default: new ServicioSaludoItaliano();
        }
        return servicio;
    }
}