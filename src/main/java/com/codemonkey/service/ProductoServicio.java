package com.codemonkey.service;

import com.codemonkey.clases.Producto;
import java.util.List;

public interface ProductoServicio{
    public abstract List<Producto> getAllProductos();
    public abstract Producto getProductoById(Integer id);
    public abstract Producto saveOrUpdateProducto(Producto producto);
    public abstract void borrarProducto(Integer id);
}