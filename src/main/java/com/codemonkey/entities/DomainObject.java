package com.codemonkey.entities;

public interface DomainObject {
    public abstract Integer getId();
    public abstract void setId(Integer id);
}