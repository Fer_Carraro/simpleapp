package com.codemonkey.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.codemonkey.service.HolaMundoServicio;

@Component
public class InyectarPorConstructorServicio{
	private HolaMundoServicio holaMundoServicio;

	@Autowired
	public InyectarPorConstructorServicio(HolaMundoServicio holaMundoServicio){
		this.holaMundoServicio = holaMundoServicio;
	}

	public void getMensaje(String nombre){
		holaMundoServicio.decirHola(nombre);
	}

} 