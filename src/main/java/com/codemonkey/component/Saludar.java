package com.codemonkey.component;

import org.springframework.stereotype.Component;

import static java.lang.System.out;

@Component
public class Saludar{
	public void diHola(){
    	out.println("Programando con Spring Framework");
    	out.println("Usuario: "+System.getProperty("user.name"));
    	out.println("Home: "+System.getProperty("user.home"));
    	out.println("Directorio: "+System.getProperty("user.dir"));
    	out.println("Arch OS: "+System.getProperty("os.arch"));
    	out.println("Name OS: "+System.getProperty("os.name"));
    	out.println("OS version: "+System.getProperty("os.version"));
    	out.println("Java home: "+System.getProperty("java.home"));
    	out.println("Java vendor: "+System.getProperty("java.vendor.url"));
    	out.println("Java version: "+System.getProperty("java.version"));	
    }

    public String decirAdios(){
        return "Arrivaderci!";
    }


}