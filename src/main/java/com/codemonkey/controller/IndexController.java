package com.codemonkey.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/cursospring")
public class IndexController {

    //http://localhost:8090/cursospring/
    @RequestMapping("/")
    public String index(Model model){
        List<String> listaCursos = new ArrayList<>();
        listaCursos.add(0, new String("Spring framework for .Net developers"));
        listaCursos.add(1, new String("Android for beginners"));
        listaCursos.add(2, new String("Angular JS para principiantes"));
        listaCursos.add(3, new String("Javascript avanzado para programadores .Net"));
        model.addAttribute("bienvenida","¡Bienvenido al curso de Spring Framework!");
        model.addAttribute("fecha", LocalDateTime.now());
        model.addAttribute("listaCursos", listaCursos);
        model.addAttribute("cursos", "Cursos disponibles:");
        return "index";
    }
}