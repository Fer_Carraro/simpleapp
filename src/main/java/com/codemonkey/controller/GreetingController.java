package com.codemonkey.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import static java.lang.System.out;
import com.codemonkey.service.GreetingService;
import com.codemonkey.service.ServicioSaludo;
import com.codemonkey.service.impl.ServicioSaludoAleman;
import com.codemonkey.service.ComponenteSaludo;

@Controller
public class GreetingController{

    @Autowired
    private GreetingService greetingServiceImpl;

    @Autowired
    private ComponenteSaludo componenteSaludo;

    @Autowired
    private ServicioSaludo servicioSaludo;

    @Autowired
    private ServicioSaludoAleman servicioSaludoAleman;

    public String saludar(){
        String saludo = servicioSaludo.saludar();
        String saludoAleman = servicioSaludoAleman.saludar();
        out.println(saludo);
        out.println(saludoAleman);
        return saludo;
    }

    
    public String decirHola(){
        out.println(greetingServiceImpl.getGreeting());
        return greetingServiceImpl.getGreeting();
    }

    public String decirAdios(){
        out.println(componenteSaludo.adios());
        return componenteSaludo.adios();
    }

}