package com.codemonkey.controller;

import com.codemonkey.clases.Producto;
import com.codemonkey.service.ProductoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/cursospring")
public class ProductoController {

    private ProductoServicio productoServicio;

    @Autowired
    public void setProductoServicio(ProductoServicio productoServicio) {
        this.productoServicio = productoServicio;
    }

    //http://localhost:8090/cursospring/productos
    @RequestMapping("/productos")
    public String listaProductos(Model model){
        model.addAttribute("productos", productoServicio.getAllProductos());
        return "productos";
    }

    //http://localhost:8090/cursospring/producto/1
    @RequestMapping("/producto/{id}")
    public String getProducto(@PathVariable Integer id,Model model){
        model.addAttribute("producto",productoServicio.getProductoById(id));
        return "producto";
    }

    //http://localhost:8090/cursospring/producto/editar/1
    @RequestMapping("producto/editar/{id}")
    public String editar(@PathVariable Integer id, Model model){
        model.addAttribute("producto", productoServicio.getProductoById(id));
        return "productoform";
    }

    //http://localhost:8090/cursospring/producto/nuevo
    @RequestMapping("/producto/nuevo")
    public String nuevoProducto(Model model){
        model.addAttribute("producto",new Producto());
        return "productoform";
    }

    //http://localhost:8090/cursospring/producto
    //@RequestMapping(value = "/producto", method = RequestMethod.POST)
    @PostMapping("/producto")
    public String saveOrUpdateProducto(Producto producto){
        Producto savedProducto = productoServicio.saveOrUpdateProducto(producto);
        return "redirect:/cursospring/producto/" + savedProducto.getId();
    }

    @RequestMapping("/producto/borrar/{id}")
    public String dborrar(@PathVariable Integer id){
        productoServicio.borrarProducto(id);
        return "redirect:/cursospring/productos";
    }

}