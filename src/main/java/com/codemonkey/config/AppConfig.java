package com.codemonkey.config;
 
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
 
import com.codemonkey.service.MyService;
import com.codemonkey.service.impl.MyServiceImpl;
 
@Configuration
public class AppConfig {
 
    @Bean(name="myServiceBean")
    public MyService myService() {
        return new MyServiceImpl();
    }
}