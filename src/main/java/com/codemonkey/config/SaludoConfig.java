package com.codemonkey.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import com.codemonkey.service.SaludoFactory;
import com.codemonkey.service.ServicioSaludo;
import com.codemonkey.service.impl.ServicioSaludoAleman;
import com.codemonkey.service.impl.ServicioSaludoItaliano;

/** 
 * @Configuration -> Índica que uno o mas Beans van
 * a ser procesados
*/

@Configuration
public class SaludoConfig{

    @Bean
    public SaludoFactory saludoFactory(){
        return new SaludoFactory();
    }

    @Bean
    @Profile("german")
    @Primary
    public ServicioSaludo crearServicioSaludoAleman(SaludoFactory factory){
        return factory.crearSaludoServicio("de");
    }

    @Bean
    @Profile("italian")
    @Primary
    public ServicioSaludo crearServicioSaludoItaliano(SaludoFactory factory){
        return factory.crearSaludoServicio("it");
    }

    @Bean(name = "french")
    public ServicioSaludo crearServicioSaludoFrances(SaludoFactory factory){
        return factory.crearSaludoServicio("fr");
    }

    @Bean
    public ServicioSaludo crearServicioSaludoPortugues(SaludoFactory factory){
        return factory.crearSaludoServicio("pt");
    }


    /**
     * 
     * @Bean -> Índica que el método va a producir un Bean 
     */
    @Bean
    @Profile("german")
    public ServicioSaludo servicioSaludoAleman(){
        return new ServicioSaludoAleman();
    }

    @Bean
    @Profile("italian")
    public ServicioSaludo servicioSaludoItaliano(){
        return new ServicioSaludoItaliano();
    }
}