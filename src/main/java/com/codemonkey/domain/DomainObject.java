package com.codemonkey.domain;

public interface DomainObject {
    public abstract Integer getId();
    public abstract void setId(Integer id);
}