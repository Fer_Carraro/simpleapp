package com.codemonkey.clases;

public class Ropa{
	private String talla;
	private String color;

	public Ropa(){}

	public Ropa(String talla, String color){
		this.talla = talla;
		this.color = color;
	}

	public String getTalla(){
		return talla;
	}

	public void setTalla(String talla){
		this.talla = talla;
	}

	public String getColor(){
		return color;
	}

	public void setColor(String color){
		this.color = color;
	}

	@Override public String toString(){
		return "Ropa{talla = "+talla+", color = "+color+"}";
	}


}