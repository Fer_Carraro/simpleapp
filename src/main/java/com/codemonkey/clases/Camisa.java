package com.codemonkey.clases;

public class Camisa extends Ropa{
	private String marca;

	public Camisa(){}

	public Camisa(String talla, String color){
		super(talla,color);
		this.marca = marca;
	}

	public Camisa(String marca, String talla, String color){
		this(talla,color);
		this.marca = marca;
	}

	public String getMarca(){
		return marca;
	}

	public void setMarca(String marca){
		this.marca = marca;
	}

	@Override public String toString(){
		return super.toString()+"::Camisa{marca = "+marca+"}";
	}
}