package com.codemonkey.clases;

import java.math.BigDecimal;

public class Producto{
    private Integer id;
    private String descripcion;
    private BigDecimal precio;
    private String imagenUrl;

    public Producto(){}
    
    public Producto(Integer id, String descripcion,BigDecimal precio,String imagenUrl){
        this.id=id;
        this.descripcion=descripcion;
        this.precio=precio;
        this.imagenUrl=imagenUrl;
    }

    public Integer getId(){
        return id;
    }

    public void setId(Integer id){
        this.id=id;
    }

    public String getDescripcion(){
        return descripcion;
    }

    public void setDescripcion(String descripcion){
        this.descripcion=descripcion;
    }

    public BigDecimal getPrecio(){
        return precio;
    }

    public void setPrecio(BigDecimal precio){
        this.precio=precio;
    }

    public String getImagenUrl(){
        return imagenUrl;
    }

    public void setImagenUrl(String imagenUrl){
        this.imagenUrl = imagenUrl;
    }

    @Override
    public String toString(){
        return "Producto{id: "+id+", descripcion: "+descripcion+", precio: "+precio+", imagenUrl: "+imagenUrl+"}";
    }
}