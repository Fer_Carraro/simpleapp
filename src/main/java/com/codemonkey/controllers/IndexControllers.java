package com.codemonkey.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by jt on 11/6/15.
 */
@Controller
public class IndexControllers {

    @RequestMapping({"/", ""})
    public String index(){
        return "index";
    }
}
