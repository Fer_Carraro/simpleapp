package com.codemonkey.controllers;

import com.codemonkey.domains.Products;
import com.codemonkey.services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by jt on 11/6/15.
 */
@Controller
public class ProductControllers {

    private ProductServices productService;

    @Autowired
    public void setProductService(ProductServices productService) {
        this.productService = productService;
    }

    @RequestMapping("/products/list")
    public String listProducts(Model model){
        model.addAttribute("products", productService.listAll());
        return "products/list";
    }

    @RequestMapping("/products/show/{id}")
    public String getProduct(@PathVariable Integer id, Model model){
        model.addAttribute("product", productService.getById(id));
        return "products/show";
    }

    @RequestMapping("products/edit/{id}")
    public String edit(@PathVariable Integer id, Model model){
        model.addAttribute("product", productService.getById(id));
        return "products/productform";
    }

    @RequestMapping("/products/new")
    public String newProduct(Model model){
        model.addAttribute("product", new Products());
        return "products/productform";
    }

    @RequestMapping(value = "/products", method = RequestMethod.POST)
    public String saveOrUpdateProduct(Products product){
        Products savedProduct = productService.saveOrUpdate(product);
        return "redirect:/products/show/" + savedProduct.getId();
    }

    @RequestMapping("/products/delete/{id}")
    public String delete(@PathVariable Integer id){
        productService.delete(id);
        return "redirect:/products/list";
    }
}
