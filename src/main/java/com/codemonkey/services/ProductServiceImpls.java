package com.codemonkey.services;

import com.codemonkey.domains.DomainObjects;
import com.codemonkey.domains.Products;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jt on 11/6/15.
 */
@Service
public class ProductServiceImpls extends AbstractMapServices implements ProductServices {

    @Override
    public List<DomainObjects> listAll() {
        return super.listAll();
    }

    @Override
    public Products getById(Integer id) {
        return (Products) super.getById(id);
    }

    @Override
    public Products saveOrUpdate(Products domainObject) {
        return (Products) super.saveOrUpdate(domainObject);
    }

    @Override
    public void delete(Integer id) {
        super.delete(id);
    }

    protected void loadDomainObjects(){
        domainMap = new HashMap<>();

        Products product1 = new Products();
        product1.setId(1);
        product1.setDescription("Product 1");
        product1.setPrice(new BigDecimal("12.99"));
        product1.setImageUrl("http://example.com/product1");

        domainMap.put(1, product1);

        Products product2 = new Products();
        product2.setId(2);
        product2.setDescription("Product 2");
        product2.setPrice(new BigDecimal("14.99"));
        product2.setImageUrl("http://example.com/product2");

        domainMap.put(2, product2);

        Products product3 = new Products();
        product3.setId(3);
        product3.setDescription("Product 3");
        product3.setPrice(new BigDecimal("34.99"));
        product3.setImageUrl("http://example.com/product3");

        domainMap.put(3, product3);

        Products product4 = new Products();
        product4.setId(4);
        product4.setDescription("Product 4");
        product4.setPrice(new BigDecimal("44.99"));
        product4.setImageUrl("http://example.com/product4");

        domainMap.put(4, product4);

        Products product5 = new Products();
        product5.setId(5);
        product5.setDescription("Product 2");
        product5.setPrice(new BigDecimal("25.99"));
        product5.setImageUrl("http://example.com/product5");

        domainMap.put(5, product5);
    }
}
