package com.codemonkey.services;

import java.util.List;

/**
 * Created by jt on 11/14/15.
 */
public interface CRUDServices<T> {
    List<?> listAll();

    T getById(Integer id);

    T saveOrUpdate(T domainObject);

    void delete(Integer id);
}
