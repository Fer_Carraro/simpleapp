package com.codemonkey.services;

import com.codemonkey.domains.DomainObjects;

import java.util.*;

/**
 * Created by jt on 11/14/15.
 */
public abstract class AbstractMapServices  {
    protected Map<Integer, DomainObjects> domainMap;

    public AbstractMapServices() {
        domainMap = new HashMap<>();
        loadDomainObjects();
    }

    public List<DomainObjects> listAll() {
        return new ArrayList<>(domainMap.values());
    }

    public DomainObjects getById(Integer id) {
        return domainMap.get(id);
    }

    public DomainObjects saveOrUpdate(DomainObjects domainObject) {
        if (domainObject != null){

            if (domainObject.getId() == null){
                domainObject.setId(getNextKey());
            }
            domainMap.put(domainObject.getId(), domainObject);

            return domainObject;
        } else {
            throw new RuntimeException("Object Can't be null");
        }
    }

    public void delete(Integer id) {
        domainMap.remove(id);
    }

    private Integer getNextKey(){
        return Collections.max(domainMap.keySet()) + 1;
    }

    protected abstract void loadDomainObjects();

}
